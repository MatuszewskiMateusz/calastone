package calastone.service;

import calastone.model.CookingHistory;
import calastone.model.Recipe;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Mateusz on 11/10/2016.
 */
@RepositoryRestResource(collectionResourceRel = "history", path = "history")
public interface CookingHistoryRepository extends PagingAndSortingRepository<CookingHistory, Long> {

}
