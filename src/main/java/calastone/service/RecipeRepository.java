package calastone.service;

import calastone.model.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Mateusz on 11/10/2016.
 */
public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long> {

    List<Recipe> findByName(@Param("name") String name);

    //native query
    @Query(value =  "SELECT * FROM RECIPE R " +
                    "WHERE R.ID NOT IN (" +
                    "     SELECT DISTINCT IR.REC_ID " +
                    "     FROM INGR_REC IR " +
                    "     INNER JOIN INGREDIENT ING ON IR.INGR_ID = ING.ID AND ING.AVAILABLE_AMOUNT < 1" +
                    ")", nativeQuery = true)
    List<Recipe> canBeCookedList();

    Page canBeCooked(Pageable p);

}
