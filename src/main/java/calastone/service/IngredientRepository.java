package calastone.service;

import calastone.model.Ingredient;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Mateusz on 11/10/2016.
 *
 * The ingredients are avilable at the addrress: http://localhost:8080/api/ingredients/?page=2&size=2
 * (page, size, sort attributes are optional)
 */
public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {

    /**
     * The method allows to find ingredient by name:
     *
     * Example usage:
     * http://localhost:8080/api/ingredients/search/findByName?name=tomato
     *
     * @param name name of the ingredient
     * @return list of ingredients
     */
    List<Ingredient> findByName(@Param("name") String name);

}
