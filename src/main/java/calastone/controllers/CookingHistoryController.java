package calastone.controllers;

import calastone.model.CookingHistory;
import calastone.model.Ingredient;
import calastone.model.Recipe;
import calastone.service.CookingHistoryRepository;
import calastone.service.IngredientRepository;
import calastone.service.RecipeRepository;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mateusz on 12/10/2016.
 */
@RestController
@RequestMapping("/api/CookingHistory/{id}")
public class CookingHistoryController {

    @Autowired
    private CookingHistoryRepository cookingHistoryRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @RequestMapping(value = "/cook", method = RequestMethod.POST)
    public CookingHistory cook(@PathVariable String id) {
        Recipe r = recipeRepository.findOne(Long.parseLong(id));
        CookingHistory entry = new CookingHistory(r);
        return cookingHistoryRepository.save(entry);
    }
}
