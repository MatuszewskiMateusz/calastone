package calastone.controllers;

import calastone.model.Ingredient;
import calastone.service.IngredientRepository;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Mateusz on 12/10/2016.
 */
@RestController
@RequestMapping("/api/ingredient/{id}")
public class IngredientController {

    @Autowired
    private IngredientRepository ingredientRepository;

    @RequestMapping(value = "/increment", method = RequestMethod.POST)
    public Ingredient increment(@PathVariable String id) {
        Ingredient i = ingredientRepository.findOne(Long.parseLong(id));
        i.setAvailableAmount(i.getAvailableAmount()+1);
        return ingredientRepository.save(i);
    }

    @RequestMapping(value = "/decrement",method = RequestMethod.POST)
    @JsonDeserialize
    public Ingredient decrement(@PathVariable  String id) {
        Ingredient i = ingredientRepository.findOne(Long.parseLong(id));
        i.setAvailableAmount(i.getAvailableAmount()-1);
        return ingredientRepository.save(i);
    }



}
