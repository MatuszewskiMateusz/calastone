package calastone.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Mateusz on 11/10/2016.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
/**
 * apparently using native query with default sorting&paging mechanism requires a bit more effort
 */
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "SqlResultSetMapping.count", columns = @ColumnResult(name = "cnt"))
})
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Recipe.canBeCooked",
                resultClass = Recipe.class,
                query =     "SELECT * FROM RECIPE R " +
                            "WHERE R.ID NOT IN (" +
                            "     SELECT DISTINCT IR.REC_ID " +
                            "     FROM INGR_REC IR " +
                            "     INNER JOIN INGREDIENT ING ON IR.INGR_ID = ING.ID AND ING.AVAILABLE_AMOUNT < 1" +
                            ")"
        ),
        @NamedNativeQuery(
                name = "Recipe.canBeCooked.count",
                resultSetMapping = "SqlResultSetMapping.count",
                query = "SELECT count(*) AS cnt FROM RECIPE R " +
                        "WHERE R.ID NOT IN (" +
                        "     SELECT DISTINCT IR.REC_ID " +
                        "     FROM INGR_REC IR " +
                        "     INNER JOIN INGREDIENT ING ON IR.INGR_ID = ING.ID AND ING.AVAILABLE_AMOUNT < 1" +
                        ")"
        )
})
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany
    @JoinTable(
            name="INGR_REC",
            joinColumns = @JoinColumn(name="REC_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name="INGR_ID", referencedColumnName = "ID")
    )
    private List<Ingredient> requiredIngredients;

    public Recipe(String name) {
        this.name = name;
    }
}
