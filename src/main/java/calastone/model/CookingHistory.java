package calastone.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Mateusz on 11/10/2016.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class CookingHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date cookingDate;

    @ManyToOne
    private Recipe recipe;

    @PrePersist
    public void prePersist() {
        this.cookingDate = new Date();
    }

    public CookingHistory(Recipe recipe) {
        this.recipe = recipe;
    }

}
