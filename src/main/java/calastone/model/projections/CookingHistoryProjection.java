package calastone.model.projections;

import calastone.model.CookingHistory;
import calastone.model.Recipe;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

/**
 * Created by Mateusz on 11/10/2016.
 */
@Projection(name = "details", types = CookingHistory.class)
public interface CookingHistoryProjection {
    Date getCookingDate();
    Recipe getRecipe();
}