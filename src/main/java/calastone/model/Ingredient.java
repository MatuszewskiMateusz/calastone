package calastone.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Mateusz on 11/10/2016.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private int availableAmount;

    public Ingredient(String name, int availableAmount) {
        this.name = name;
        this.availableAmount = availableAmount;
    }
}
