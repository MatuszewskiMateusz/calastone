insert into ingredient(name, available_amount) values('egg', 1)
insert into ingredient(name, available_amount) values('tomato', 1)
insert into ingredient(name, available_amount) values('courgette', 1)
insert into ingredient(name, available_amount) values('pork', 1)
insert into ingredient(name, available_amount) values('chilli', 1)

insert into recipe(name) values('veggie tomato')
insert into recipe(name) values('veggie courgette')
insert into recipe(name) values('meaty')
insert into recipe(name) values('meaty spicy')

insert into INGR_REC(INGR_ID, REC_ID) values(1, 1)
insert into INGR_REC(INGR_ID, REC_ID) values(2, 1)

insert into INGR_REC(INGR_ID, REC_ID) values(1, 2)
insert into INGR_REC(INGR_ID, REC_ID) values(3, 2)

insert into INGR_REC(INGR_ID, REC_ID) values(1, 3)
insert into INGR_REC(INGR_ID, REC_ID) values(2, 3)
insert into INGR_REC(INGR_ID, REC_ID) values(4, 3)

insert into INGR_REC(INGR_ID, REC_ID) values(1, 4)
insert into INGR_REC(INGR_ID, REC_ID) values(2, 4)
insert into INGR_REC(INGR_ID, REC_ID) values(4, 4)
insert into INGR_REC(INGR_ID, REC_ID) values(5, 4)
