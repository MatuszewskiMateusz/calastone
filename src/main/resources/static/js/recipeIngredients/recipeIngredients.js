angular.module('recipeIngredients', []).controller('recipeIngredients', function($http, $routeParams) {
    var self = this;
    $http.get('/api/recipes/'+$routeParams.id).then(function(response) {
        self.recipe = response.data;
    });

    $http.get('/api/recipes/'+$routeParams.id+'/requiredIngredients').then(function(response) {
        self.reqIngredients = response.data.content;
    });
});
