angular.module('cookingHistory', []).controller('cookingHistory', function($http) {
    var self = this;
    $http.get('/api/history?projection=details').then(function(response) {
        self.data = response.data;
    });
});
