angular.module('ingredients', []).controller('ingredients', function($http) {
	var self = this;
	$http.get('/api/ingredients').then(function(response) {
        self.data = response.data;
	});

	self.increment = function (i) {
		var request = $http({
			method: 'POST',
			url: '/api/ingredient/'+i.id+'/increment'
		});
		request.success(function(data) {
			self.data.content[i.id-1].availableAmount = data.availableAmount;
		});
	}

	self.decrement = function (i) {
		var request = $http({
			method: 'POST',
			url: '/api/ingredient/'+i.id+'/decrement'
		});
		request.success(function(data) {
			self.data.content[i.id-1].availableAmount = data.availableAmount;
		});
	}
});
