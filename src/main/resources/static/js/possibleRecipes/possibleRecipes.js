angular.module('possibleRecipes', []).controller('possibleRecipes', function($http) {
	var self = this;
	$http.get('/api/recipes/search/canBeCooked').then(function(response) {
		self.data = response.data;
	});

	self.cook = function(id) {
		var request = $http({
			method: 'POST',
			url: '/api/CookingHistory/'+id+'/cook'
		});
	}
});
