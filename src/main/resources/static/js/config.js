angular
    .module('CalastoneApp', [ 'ngRoute', 'home', 'ingredients', 'recipes', 'possibleRecipes', 'recipeIngredients', 'navigation', 'cookingHistory'])
    .config(

        function($routeProvider, $httpProvider, $locationProvider) {
            $routeProvider.when('/', {
                templateUrl : 'js/home/home.html',
                controller : 'home',
                controllerAs : 'controller'
            }).when('/ingredients', {
                templateUrl : 'js/ingredients/ingredients.html',
                controller : 'ingredients',
                controllerAs : 'controller'
            }).when('/recipes', {
                templateUrl : 'js/recipes/recipes.html',
                controller : 'recipes',
                controllerAs : 'controller'
            }).when('/possibleRecipes', {
                templateUrl : 'js/possibleRecipes/possibleRecipes.html',
                controller : 'possibleRecipes',
                controllerAs : 'controller'
            }).when('/recipe/:id/ingredients', {
                templateUrl : 'js/recipeIngredients/recipeIngredients.html',
                controller : 'recipeIngredients',
                controllerAs : 'controller'
            }).when('/cookingHistory', {
                templateUrl : 'js/cookingHistory/cookingHistory.html',
                controller : 'cookingHistory',
                controllerAs : 'controller'
            }).otherwise('/');

            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        });
